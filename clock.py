import time
import datetime
import turtle as tu
# creating a screen object 
now = datetime.datetime.now()
screen =tu.Screen()
screen.title('WALL CLOCK')
screen.bgcolor('#7c797c')
# to make the pencils at the center
def pensetter(pen):
	pen.left(90)
	pen.penup()
	pen.forward(120)
	pen.pendown()
# creating the object of the clock
circle=tu.Turtle()
circle.pensize(5)
circle.shapesize(0.1,0.1,0.1)
circle.speed(0)
tu.tracer(1,0)
circle.fillcolor('#f2f2f2')
circle.begin_fill()
for i in range (360):
	circle.forward(2)
	circle.left(1)
	if i%30==0:
		circle.left(90)
		circle.forward(15)
		circle.backward(15)
		circle.right(90)
	elif i%6==0:
		circle.pensize(2)
		circle.left(90)
		circle.forward(10)
		circle.backward(10)
		circle.right(90)
		circle.pensize(5)
circle.end_fill()
tu.tracer(1,0)
second_hand=tu.Turtle()
minute_hand=tu.Turtle()
hour_hand=tu.Turtle()
pensetter(second_hand)
pensetter(minute_hand)
pensetter(hour_hand)
hand=[second_hand,minute_hand,hour_hand]
setter=2
for i in hand:
	i.speed(0)
	i.pensize(setter)
	i.hideturtle()
	setter=setter+2
#adjusting the turtle with the date time
hour =0
if now.hour>12:
	hour=now.hour-12
else:
	hour=now.hour
hour_hand.right(hour*30)
hour_hand.forward(50)
minute_hand.right(now.minute*6)
minute_hand.forward(70)
second_hand.right(now.second*6)
for i in range (60-now.second):
	second_hand.forward(90)
	time.sleep(0.99)
	second_hand.backward(90)
	second_hand.right(6)
	second_hand.clear()
hour_hand.backward(50)
minute_hand.backward(70)
minute_hand.clear()
hour_hand.clear()
minute_hand.right(6)
while True:
	hour_hand.forward(50)
	for j in range(60):
		minute_hand.forward(70)
		for i in range (60):
			second_hand.forward(90)
			time.sleep(0.99)
			second_hand.backward(90)
			second_hand.right(6)
			second_hand.clear()
		minute_hand.backward(70)
		minute_hand.right(6)
		minute_hand.clear()
	hour_hand.backward(50)
	hour_hand.right(6)
	hour_hand.clear()
